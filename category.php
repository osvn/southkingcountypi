<?php
global $osvn_opt;
get_header();
echo '
<div class="full">
    <div class="container">
        <div id="index">
';
				if ( have_posts() ) // Neu co bai viet
				{
					echo '<h1 class="page-title">';
					single_cat_title();
					echo '</h1>';


					while ( have_posts() ) : the_post();

						get_template_part( 'content' );

					endwhile;

				} else { // Neu khong co bai viet

					get_template_part( 'content', 'none' );

				}
echo '</div></div></div>';
get_footer();