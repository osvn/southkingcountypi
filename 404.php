<?php
get_header();
?>
<div id="main" class="full">
    <div class="container">
		<div class="content content-single">
			<div class="text-content">
					<h1 class="title">Not Found</h1>
						<div class="content">
							<?php get_template_part( 'content', 'none' );?>
						</div>
				<?php endwhile;?>
			</div>
		</div>
	</div>
</div>
<?php get_footer();?>