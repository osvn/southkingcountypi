<?php
global $osvn_opt;
?>
<footer>
		<div class="top">
			<div class="container">
				<?php echo $osvn_opt['footer_text'];?>
			</div>
		</div>
		<div class="bottom">
			<div class="container">
				<div class="fl">&copy; HUMISTON INVESTIGATIONS <?php echo date('Y');?></div>
				<div class="fr">WEB DESIGN BY <a href="http://creativehaus.com/" target="_blank">CREATIVEHAUS</a></div>
			</div>
		</div>
    </footer><!--/footer -->
	
</div>
<?php wp_footer();?>
<?php if(is_home() || is_front_page()){?>
	<script>
		jQuery(document).ready(function(){
			/* Perfect Accordition */
			jQuery("#index .right ul").carouFredSel({
				items               : 3,
				direction           : "up",
				next			: {
							button			: '#carousel_next',
							key				: 'up'
						},
				prev			: {
							button			: '#carousel_prev',
							key				: 'down'
					},
				scroll : {
					items           : 1,
					easing          : "swing",
					duration        : 400,                        
					pauseOnHover    : true
				}
			});
		});
	</script>
<?php }?>
<script>
jQuery('.wpcf7-form br').remove();
</script>
<script><?php if(isset($osvn_opt['footer_script']) && !empty($osvn_opt['footer_script'])){echo $osvn_opt['footer_script'];} ?></script>
</body>
</html>
