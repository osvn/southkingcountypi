<?php
function osvn_custom_dashboard_widgets() {
	global $wp_meta_boxes;

	wp_add_dashboard_widget( 'custom_help_widget', 'Welcome', 'custom_dashboard_help' );
}

function custom_dashboard_help() {
	?>
	<p class="about-description"><?php _e( 'This website was designed and developed by ' ); ?><a
			href="http://creativehaus.com/" target="_blank"><?php echo _e( 'Creativehaus.com' ); ?></a></p>
	<p></p>
	<p><a href="http://creativehaus.com/" target="_blank"><img
				src="<?php echo OSVN_IMG . '/logo-creativehaus.png'; ?>"></a></p>
	<p></p>
	<div class="">
		<p class="about-description">If you are currently on, or interested in maintenance, please contact us.</p>

		<p><strong>Contact Number: 1-888-966-7265. Support:</strong> <a href="http://creativehaus.com/contact/"
		                                                                target="_blank">Click here</a></p>
	</div>
<?php
}

function osvn_title() {
	global $paged, $s;
	if ( ! function_exists( 'wpseo_init' ) ) {
		if ( function_exists( 'is_tag' ) && is_tag() ) {
			single_tag_title( __( 'Tag Archive for &quot;', 'osvn' ) );
			_e( '&quot; - ', 'osvn' );
		} elseif ( is_archive() ) {
			wp_title();
			_e( ' Archive - ', 'osvn' );
		} elseif ( is_search() ) {
			_e( 'Search for &quot;', 'osvn' ) . htmlspecialchars( $s ) . _e( '&quot; - ', 'osvn' );
		} elseif ( ! ( is_404() ) && ( is_single() ) || ( is_page() ) ) {
			wp_title();
			_e( ' - ', 'osvn' );
		} elseif ( is_404() ) {
			_e( 'Not Found - ', 'osvn' );
		}

		if ( is_home() ) {
			bloginfo( 'name' );
			_e( ' - ', 'osvn' );
			bloginfo( 'description' );
		} else {
			bloginfo( 'name' );
		}

		if ( $paged > 1 ) {
			_e( ' - page ', 'osvn') . $paged; }
	} else {
		wp_title();
	}
}
/*=================logo=============================*/
function osvn_logo(){
    global $osvn_opt;
    if(isset($osvn_opt['logo']['url']) && !empty($osvn_opt['logo']['url'])){
    	echo '<a href="'.esc_url( home_url( '/' ) ).'" class="logo fl"><img src="'.$osvn_opt['logo']['url'].'" alt="'.get_bloginfo('name').'" class="res-img"/></a>';
    }else{
    	echo '<a href="'.esc_url( home_url( '/' ) ).'" class="logo fl"><img src="'.OSVN_IMG.'/logo.png" alt="'.get_bloginfo('name').'" class="res-img" /></a>';
    }
    
}
/*=================phone=============================*/
function osvn_hotline(){
	global $osvn_opt;
	echo '
	<ul class="fr call-list">
	';
		if(isset($osvn_opt['hotline']) && !empty($osvn_opt['hotline'])){
			$string = $osvn_opt['hotline'];
			$pattern = '/[^0-9]*/';
			$result = preg_replace($pattern,'', $string);
			echo '<li><a class="phone" href="tel:'.$result.'">'.$string.'</a></li>';
		}
		if(isset($osvn_opt['osvn-fax']) && !empty($osvn_opt['osvn-fax'])){
			echo '<li><a href="javascript:void(0)" class="fax">'.$osvn_opt['osvn-fax'].'</a></li>';		
		}
	echo '</ul>';
}
/*=================email=============================*/
function osvn_email(){
	global $osvn_opt;
	if(isset($osvn_opt['email-contact']) && !empty($osvn_opt['email-contact'])){
		echo '<a href="mailto:'.$osvn_opt['email-contact'].'" id="email"><img src="'.OSVN_IMG.'/email.jpg" alt="" /></a>';
	}
}
/*=================adv=============================*/
function osvn_advertise(){
	global $osvn_opt;
	if(isset($osvn_opt['adv-header']['url']) && !empty($osvn_opt['adv-header']['url'])){
		echo '<a class="pull-right adv" href="'.$osvn_opt['adv-header-link'].'" target="_blank"><img src="'.$osvn_opt['adv-header']['url'].'"></a>';
	}
}
/*==================slider==========================================*/
function osvn_slider(){
	$args = array('post_type'=>'osvn_slide');
	$wq = new WP_Query($args);
	if($wq->have_posts()){
		echo '
		<div id="slider">
			<div id="carousel" class="carousel slide" data-ride="carousel">
		';
			echo '<ol class="carousel-indicators animation-all">';
				$a = 0;
				while($wq->have_posts()){ $wq->the_post();
					if($a == 0){
						$active_a = 'active';
					}else{
						$active_a = '';
					}
					echo '<li data-target="#carousel" data-slide-to="'.$a.'" class="'.$active_a.'"></li>';
					$a++;
				}
			echo '</ol>';
			echo '<div class="carousel-inner" role="listbox">';
			$b = 0;
			while($wq->have_posts()){ $wq->the_post();
				if($b == 0){
					$active = 'active';
				}else{
					$active = '';
				}
				$link = get_post_meta(get_the_ID(), '_osvn_slider_link', true);
				if(isset($link) && !empty($link)){
					$img = '<a href="'.$link.'">'.get_the_post_thumbnail(get_the_ID(), 'slider').'</a>';
				}else{
					$img = get_the_post_thumbnail(get_the_ID(), 'slider');
				}
				echo '<div class="item '.$active.'">';
					echo $img;
				echo '</div>';
				$b++;
			}
			echo '</div>';
		echo '
			</div>
		</div>
		';
	}wp_reset_query();
}
/*==================get style,pattern,color... product====================*/
function osvn_get_term($taxonomy, $tax_name){
	$args = array(
						    'orderby'           => 'id', 
						    'order'             => 'ASC',
						    'hide_empty'        => false, 
						);
	$terms = get_terms( $taxonomy, $args );
	//var_dump($terms);
	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
		echo '
		<h4>'.$tax_name.'</h4>
		<ul class="list">
		';
		foreach ( $terms as $term ) {
			echo '<li><a href="' . get_term_link( $term ) . '">' . $term->name . ' (' . $term->count . ')</a></li>';
		}
		echo '
		</ul>
		';
	}

}
/*==========================login with email or user name=====================================*/
function login_with_email_address($username) {
        $user = get_user_by('email',$username);
        if(!empty($user->user_login))
                $username = $user->user_login;
        return $username;
}
add_action('wp_authenticate','login_with_email_address');
/*===================add class widget====================*/
function slug_widget_order_class() {
	global $wp_registered_sidebars, $wp_registered_widgets;

	// Grab the widgets
	$sidebars = wp_get_sidebars_widgets();

	if ( empty( $sidebars ) )
	return;

	// Loop through each widget and change the class names
	foreach ( $sidebars as $sidebar_id => $widgets ) {
		if ( empty( $widgets ) )
		continue;
		$number_of_widgets = count( $widgets );
		foreach ( $widgets as $i => $widget_id ) {
		$wp_registered_widgets[$widget_id]['classname'] .= ' widget-order-' . $i;

		// Add first widget class
		if ( 0 == $i ) {
		$wp_registered_widgets[$widget_id]['classname'] .= ' first_main_menu_item selected_main_menu_item';
		}

		// Add last widget class
		if ( $number_of_widgets == ( $i + 1 ) ) {
		$wp_registered_widgets[$widget_id]['classname'] .= ' last_main_menu_item';
		}
		}
	}
}
add_action( 'init', 'slug_widget_order_class' );
/*===================Get the Latest Post of a Facebook Page==========================*/
function osvn_time_ago($timestamp) 
{
	$diff = time() - (int) $timestamp;

		if ($diff == 0) 
			return __('just now', "recent-facebook-posts");

		$intervals = array
		(
			1                   => array('year',    31556926),
			$diff < 31556926    => array('month',   2628000),
			$diff < 2629744     => array('week',    604800),
			$diff < 604800      => array('day',     86400),
			$diff < 86400       => array('hour',    3600),
			$diff < 3600        => array('minute',  60),
			$diff < 60          => array('second',  1)
			);

		$value = floor($diff / $intervals[1][1]);

		$time_unit = $intervals[1][0];

		switch($time_unit) {
			case 'year':
				return sprintf(_n('1 year ago', '%d years ago', $value, "recent-facebook-posts"), $value); 
			break;

			case 'month':
				return sprintf(_n('1 month ago', '%d months ago', $value, "recent-facebook-posts"), $value); 
			break;

			case 'week':
				return sprintf(_n('1 week ago', '%d weeks ago', $value, "recent-facebook-posts"), $value); 
			break;

			case 'day':
				return sprintf(_n('1 day ago', '%d days ago', $value, "recent-facebook-posts"), $value); 
			break;

			case 'hour':
				return sprintf(_n('1 hour ago', '%d hours ago', $value, "recent-facebook-posts"), $value); 
			break;

			case 'minute':
				return sprintf(_n('1 minute ago', '%d minutes ago', $value, "recent-facebook-posts"), $value); 
			break;

			case 'second':
				return sprintf(_n('1 second ago', '%d seconds ago', $value, "recent-facebook-posts"), $value); 
			break;

			default:
				return sprintf(__('Some time ago', "recent-facebook-posts")); 
			break;
		}
		
		
}
function osvn_feacebook_feed(){
	global $osvn_opt;
	if(!empty($osvn_opt['facebook-access-token']) && !empty($osvn_opt['facebook-id-slug'])){
	
		$graph_url = 'https://graph.facebook.com/'.$osvn_opt['facebook-id-slug'].'/posts?limit=15&access_token='.$osvn_opt['facebook-access-token'].'';	
		$fb_posts = json_decode(file_get_contents($graph_url), true);
		$i = 0;
		//var_dump($fb_posts['data']);die;
		if($fb_posts){
			echo '
			<h4>Facebook Feed<span id="carousel_next" class="caroul"></span><span id="carousel_prev" class="caroul"></span></h4>
			<ul>
			';
			foreach($fb_posts['data'] as $fb_post){
				if(isset($fb_post['message']) && !empty($fb_post['message'])){
					$fb_content = $fb_post['message'];
				}else{
					$fb_content = $fb_post['caption'];
				}
				$timestamp = strtotime($fb_post['created_time']);
				echo '
				    <li>
						<a href="'.$fb_post['link'].'" target="_blank">'.$fb_content.'</a><br>
						<span class="date">'.osvn_time_ago($timestamp).'</span>
					</li>
				    ';

			    $i++;
			}
			echo '</ul>';
		}
		
	}else{
		echo '<strong style="color:#FF0000;">Please fill App ID or APP SECRET or Facebook Page ID</strong>';
	}
}
?> 