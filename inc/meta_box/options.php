<?php
//https://github.com/webdevstudios/Custom-Metaboxes-and-Fields-for-WordPress/wiki
function osvn_metaboxes( $meta_boxes ) {
    $prefix = '_osvn_'; // Prefix for all fields
    /////////////////////////////////////////
    $tt_pages = array();
    $tt_pages_obj = get_pages('sort_column=post_parent,menu_order');    
    foreach ($tt_pages_obj as $tt_page) {
    $tt_pages[$tt_page->ID] = $tt_page->post_title; }
    /////////////////////////////////////////
    $tt_terms = array();
    $tt_terms_obj = get_terms('faq_cat');    
    foreach ($tt_terms_obj as $tt_term) {
    $tt_terms[$tt_term->slug] = $tt_term->name; }
    
    
    
    
    
    
    $meta_boxes[] = array(
        'id' => 'osvn_metabox_page',
        'title' => __( 'Page description', 'osvn' ),
        'pages' => array('page'), // post type
        //'show_on' => array( 'key' => 'page-template', 'value' => 'page-templates/home.php' ),
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            
            array(
                'name' => 'Description',
                //'desc' => 'field description (optional)',
                //'default' => 'standard value (optional)',
                'id' => $prefix . 'page_des',
                'type' => 'textarea_code'
            ),
            
        ),
    );
    $meta_boxes['_home_box'] = array(
        'id'         => '_home_box',
        'title'      => __( 'Section #1: 3 box', 'cmb' ),
        'pages'      => array( 'page', ),
        'show_on' => array( 'key' => 'page-template', 'value' => 'page-templates/home.php' ),
        'fields'     => array(
            array(
                'id'          => $prefix . 'home_box',
                'type'        => 'group',
                'description' => __( 'Add', 'cmb' ),
                'options'     => array(
                    'group_title'   => __( 'Box {#}', 'cmb' ), // {#} gets replaced by row number
                    'add_button'    => __( 'Add', 'cmb' ),
                    'remove_button' => __( 'Remove', 'cmb' ),
                    'sortable'      => true, // beta
                ),
                // Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
                'fields'      => array(


                    array(
                        'name'    => 'Box title',
                        //'desc'    => 'Select an option',
                        'id'      => $prefix . 'home_box_title',
                        'type'    => 'textarea_code',
                    ),
                    array(
                        'name'    => 'Box link',
                        //'desc'    => 'Select an option',
                        'id'      => $prefix . 'home_box_link',
                        'type'    => 'text',
                    ),
                    
                ),
            ),
        ),
    );
    $meta_boxes[] = array(
        'id' => 'osvn_metabox_home_2',
        'title' => __( 'Section #2', 'osvn' ),
        'pages' => array('page'), // post type
        'show_on' => array( 'key' => 'page-template', 'value' => 'page-templates/home.php' ),
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            
            array(
                'name' => 'Title',
                //'desc' => 'field description (optional)',
                //'default' => 'standard value (optional)',
                'id' => $prefix . 'home2_box_title',
                'type' => 'textarea_code'
            ),
            array(
                'name' => 'Link',
                //'desc' => 'field description (optional)',
                //'default' => 'standard value (optional)',
                'id' => $prefix . 'home2_box_url',
                'type' => 'text'
            ),
            array(
                'name' => 'Description',
                //'desc' => 'field description (optional)',
                //'default' => 'standard value (optional)',
                'id' => $prefix . 'home2_box_des',
                'type' => 'wysiwyg'
            ),
            
            
            
        ),
    );
    $meta_boxes['_home_box3'] = array(
        'id'         => '_home_box3',
        'title'      => __( 'Section #3: membership', 'cmb' ),
        'pages'      => array( 'page', ),
        'show_on' => array( 'key' => 'page-template', 'value' => 'page-templates/home.php' ),
        'fields'     => array(
            array(
                'id'          => $prefix . 'home_box3',
                'type'        => 'group',
                'description' => __( 'Add', 'cmb' ),
                'options'     => array(
                    'group_title'   => __( 'Box {#}', 'cmb' ), // {#} gets replaced by row number
                    'add_button'    => __( 'Add', 'cmb' ),
                    'remove_button' => __( 'Remove', 'cmb' ),
                    'sortable'      => true, // beta
                ),
                // Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
                'fields'      => array(


                    array(
                        'name'    => 'Title',
                        //'desc'    => 'Select an option',
                        'id'      => $prefix . 'home3_box_title',
                        'type'    => 'textarea_code',
                    ),
                    array(
                        'name'    => 'Link',
                        //'desc'    => 'Select an option',
                        'id'      => $prefix . 'home3_box_link',
                        'type'    => 'text',
                    ),
                    array(
                        'name' => 'Image',
                        'desc' => 'Upload an image or enter an URL.',
                        'id' => $prefix . 'home3_box_image',
                        'type' => 'file',
                        'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
                    ),
                    
                ),
            ),
        ),
    );
    ////////////why hire a pi
    $meta_boxes[] = array(
        'id' => 'osvn_metabox_why',
        'title' => __( 'Page data', 'osvn' ),
        'pages' => array('page'), // post type
        'show_on' => array( 'key' => 'page-template', 'value' => 'page-templates/why.php' ),
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            
            array(
                'name' => 'Section 1',
                //'desc' => 'field description (optional)',
                //'default' => 'standard value (optional)',
                'id' => $prefix . 'why1_des',
                'type' => 'wysiwyg'
            ),
            array(
                'name' => 'Section 2',
                //'desc' => 'field description (optional)',
                //'default' => 'standard value (optional)',
                'id' => $prefix . 'why2_des',
                'type' => 'wysiwyg'
            ),
            
        ),
    );
    ////////////link
    $meta_boxes['_link'] = array(
        'id'         => '_link',
        'title'      => __( 'Add link', 'cmb' ),
        'pages'      => array( 'page', ),
        'show_on' => array( 'key' => 'page-template', 'value' => 'page-templates/link.php' ),
        'fields'     => array(
            array(
                'id'          => $prefix . '_link',
                'type'        => 'group',
                'description' => __( 'Add', 'cmb' ),
                'options'     => array(
                    'group_title'   => __( 'Box {#}', 'cmb' ), // {#} gets replaced by row number
                    'add_button'    => __( 'Add', 'cmb' ),
                    'remove_button' => __( 'Remove', 'cmb' ),
                    'sortable'      => true, // beta
                ),
                // Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
                'fields'      => array(


                    array(
                        'name'    => 'Title',
                        //'desc'    => 'Select an option',
                        'id'      => $prefix . 'link_title',
                        'type'    => 'text',
                    ),
                    array(
                        'name'    => 'Link',
                        //'desc'    => 'Select an option',
                        'id'      => $prefix . 'link_content',
                        'type'    => 'wysiwyg',
                    ),
                    
                ),
            ),
        ),
    );
    ////////////about
    $meta_boxes[] = array(
        'id' => 'osvn_metabox_about',
        'title' => __( 'Page data', 'osvn' ),
        'pages' => array('page'), // post type
        'show_on' => array( 'key' => 'page-template', 'value' => 'page-templates/about.php' ),
        'context' => 'normal',
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            
            
            array(
                'name' => 'Section bottom',
                //'desc' => 'field description (optional)',
                //'default' => 'standard value (optional)',
                'id' => $prefix . 'about_des',
                'type' => 'wysiwyg'
            ),
            
        ),
    );
    
    
    
    
    return $meta_boxes;
}
add_filter( 'cmb_meta_boxes', 'osvn_metaboxes' );