<?php
// elusive icons webfont demo: http://shoestrap.org/downloads/elusive-icons-webfont/

$this->sections[] = array(
	'icon'   => 'el-icon-cogs',
	'title'  => __( 'General Settings', 'redux-framework-demo' ),
	'fields' => array(
		array(
			'id'       => 'logo',
			'type'     => 'media',
			'title'    => __( 'Logo', 'redux-framework-demo' ),
		),
		array(
			'id'        => 'hotline',
			'type'      => 'text',
			'title'     => __('Hotline', 'osvn'),
			'default'   => '1.253.929.8371',
		),
		array(
			'id'        => 'osvn-fax',
			'type'      => 'text',
			'title'     => __('Fax number', 'osvn'),
			'default'   => '1.253.545.0617',
		),
		array(
			'id'       => 'header_script',
			'type'     => 'ace_editor',
			'mode'     => 'javascript',
			'title'    => __( 'Header Scipt', 'redux-framework-demo' ),
		),
		
		array(
			'id'       => 'footer_text',
			'type'     => 'editor',
			'title'    => __( 'Footer Text', 'redux-framework-demo' ),
			'default'  => '<ul><li>Washington State Agency License # 2141</li><li>Private Investigator\'s License # 4025</li><li>Auburn City Business License:  BUS-30219</li></ul>',
		),
		array(
			'id'       => 'footer_script',
			'type'     => 'ace_editor',
			'mode'     => 'javascript',
			'title'    => __( 'Footer Scipt', 'redux-framework-demo' ),
		),

	)
);
$this->sections[] = array(
	'icon'   => 'el-icon-facebook',
	'title'  => __( 'Facebook', 'redux-framework-demo' ),
	'fields' => array(
		array(
			'id'       => 'facebook-app-id',
			'type'     => 'text',
			'title'    => __( 'App ID', 'redux-framework-demo' ),
		),
		array(
			'id'        => 'facebook-app-secret',
			'type'      => 'text',
			'title'     => __('APP SECRET', 'osvn'),
		),
		array(
			'id'        => 'facebook-id-slug',
			'type'      => 'text',
			'title'     => __('Facebook Page ID', 'osvn'),
		),
		array(
			'id'        => 'facebook-access-token',
			'type'      => 'text',
			'title'     => __('Facebook access token', 'osvn'),
			'default'   => '767384576650208|sATUuVlJdV2dg15fdvkDVaw_Vas',
		),

	)
);
$this->sections[] = array(
	'icon'      => 'el-icon-share',
	'title'     => __('Social', 'redux-framework-demo'),
	'fields'    => array(
		array(
			'id'        => 'gplus',
			'type'      => 'text',
			'title'     => __('Google Plus', 'osvn'),
			'default'   => '+Username',
		),
		array(
			'id'        => 'facebook',
			'type'      => 'text',
			'title'     => __('Facebook ID', 'osvn'),
			'default'   => 'username',
		),
		array(
			'id'        => 'twitter',
			'type'      => 'text',
			'title'     => __('Twitter ID', 'osvn'),
			'default'   => 'username',
		),
	)
);



$theme_info = '<div class="redux-framework-section-desc">';
$theme_info .= '<p class="redux-framework-theme-data description theme-uri">' . __( '<strong>Theme URL:</strong> ', 'redux-framework-demo' ) . '<a href="' . $this->theme->get( 'ThemeURI' ) . '" target="_blank">' . $this->theme->get( 'ThemeURI' ) . '</a></p>';
$theme_info .= '<p class="redux-framework-theme-data description theme-author">' . __( '<strong>Author:</strong> ', 'redux-framework-demo' ) . $this->theme->get( 'Author' ) . '</p>';
$theme_info .= '<p class="redux-framework-theme-data description theme-version">' . __( '<strong>Version:</strong> ', 'redux-framework-demo' ) . $this->theme->get( 'Version' ) . '</p>';
$theme_info .= '<p class="redux-framework-theme-data description theme-description">' . $this->theme->get( 'Description' ) . '</p>';
$tabs = $this->theme->get( 'Tags' );
if ( ! empty( $tabs ) ) {
	$theme_info .= '<p class="redux-framework-theme-data description theme-tags">' . __( '<strong>Tags:</strong> ', 'redux-framework-demo' ) . implode( ', ', $tabs ) . '</p>';
}
$theme_info .= '</div>';

if ( file_exists( dirname( __FILE__ ) . '/../README.md' ) ) {
	$this->sections['theme_docs'] = array(
		'icon'   => 'el-icon-list-alt',
		'title'  => __( 'Documentation', 'redux-framework-demo' ),
		'fields' => array(
			array(
				'id'       => '17',
				'type'     => 'raw',
				'markdown' => true,
				'content'  => file_get_contents( dirname( __FILE__ ) . '/../README.md' )
			),
		),
	);
}
$this->sections[] = array(
	'title'  => __( 'Import / Export', 'redux-framework-demo' ),
	'desc'   => __( 'Import and Export your Redux Framework settings from file, text or URL.', 'redux-framework-demo' ),
	'icon'   => 'el-icon-refresh',
	'fields' => array(
		array(
			'id'         => 'opt-import-export',
			'type'       => 'import_export',
			'title'      => 'Import Export',
			'subtitle'   => 'Save and restore your Redux options',
			'full_width' => false,
		),
	),
);

$this->sections[] = array(
	'type' => 'divide',
);

$this->sections[] = array(
	'icon'   => 'el-icon-info-sign',
	'title'  => __( 'Theme Information', 'redux-framework-demo' ),
	'desc'   => __( '<p class="description">This is the Description. Again HTML is allowed</p>', 'redux-framework-demo' ),
	'fields' => array(
		array(
			'id'      => 'opt-raw-info',
			'type'    => 'raw',
			'content' => $item_info,
		)
	),
);