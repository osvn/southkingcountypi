<?php 
// Creating the widget 
class OSVN_Widget_Product_Color extends WP_Widget {

function __construct() {
parent::__construct(
// Base ID of your widget
'OSVN_Widget_Product_Color', 

// Widget name will appear in UI
__('* OSVN Color Filter', 'wpb_widget_domain'), 

// Widget description
array( 'description' => __( 'OSVN widget color filter', 'wpb_widget_domain' ), ) 
);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );
// before and after widget arguments are defined by themes
echo $args['before_widget'];
if ( ! empty( $title ) )
echo $args['before_title'] . $title . $args['after_title'];

// This is where you run the code and display the output
?>

	<?php 
	$osvn_args = array('orderby' => 'id','hide_empty' => false);
	$terms = get_terms( 'osvn_color', $osvn_args );
	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
		echo '<ul id="osvn_color_filter">';
		foreach ( $terms as $term ) {
	?>
			<li>
				<label>
                    <input type="checkbox" name="<?php echo $term->slug;?>" value="<?php echo $term->slug;?>">
                    <?php echo $term->name;?>
                </label>
			</li>
	<?php
		} 
		echo '</ul>';
	}
	?>

<?php
echo $args['after_widget'];
}
		
// Widget Backend 
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = __( 'Color', 'osvn' );
}
// Widget admin form
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}
} // Class wpb_widget ends here

// Register and load the widget
function osvn_load_product_color_widget() {
	register_widget( 'OSVN_Widget_Product_Color' );
}
add_action( 'widgets_init', 'osvn_load_product_color_widget' );
?>