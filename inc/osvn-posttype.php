<?php 
add_action('init','osvn_client_post_type');
	function osvn_client_post_type() {

		$labels = array(

		    'name' => 'FAQs',

		    'singular_name' => 'FAQs',

		    'add_new' => 'Add FAQ',

		    'all_items' => 'FAQs',

		    'add_new_item' => 'Add FAQ',

		    'edit_item' => 'Edit FAQ',

		    'new_item' => 'Add FAQ',

		    'view_item' => 'View FAQ',

		    'search_items' => 'Search FAQ',

		    'not_found' =>  'Not found FAQ',

		    'not_found_in_trash' => 'Not found FAQs in trash',

		    'parent_item_colon' => 'Parent FAQs: ',

		    'menu_name' => 'FAQs'

		);

		

		$args = array(

			'labels' => $labels,

			'description' => "",

			'public' => true,

			'exclude_from_search' => false,

			'publicly_queryable' => true,

			'show_ui' => true, 

			'show_in_nav_menus' => true, 

			'show_in_menu' => true,

			'show_in_admin_bar' => true,

			//'menu_position' => 100,

			'menu_icon' => 'dashicons-editor-help',

			//'capability_type' => 'post',

			'hierarchical' => false,

			'supports' => array('title', 'editor'),

			'has_archive' => false,

			//'rewrite'   => array( 'slug' => 'p-showroom' ),

			'query_var' => true,

			'can_export' => true

		); 

		

		register_post_type('osvn_faq',$args);

	}
	
?>