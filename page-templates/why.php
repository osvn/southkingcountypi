<?php 
/*
Template name: Why hire a PI
*/
?>
<?php get_header();?>
<?php 
global $post;
if(has_post_thumbnail()){
?>
<div class="back-slider">
    <?php the_post_thumbnail('page-thumb', array('class'=>'res-img'));?>
</div>
<?php }?>

<div id="main">
	<?php while(have_posts()): the_post();?>
    <div class="large-title">
        <div class="container">
                <h2><?php single_post_title();?></h2>
                <?php 
                $page_des = get_post_meta( get_the_ID(), '_osvn_page_des', true );
                if(isset($page_des) && !empty($page_des)){
                    echo wpautop($page_des);
                }
                ?>
        </div>
    </div>

    
    <?php 
    $page_des1 = get_post_meta( get_the_ID(), '_osvn_why1_des', true );
    if(isset($page_des1) && !empty($page_des1)){
    ?>
    <div class="container">
		<?php echo wpautop($page_des1);?>
	</div>
	<?php }?>

    <?php 
    $page_des2 = get_post_meta( get_the_ID(), '_osvn_why2_des', true );
    if(isset($page_des2) && !empty($page_des2)){
    ?>
    <div class="info-content">
		<div class="container">
			<?php echo wpautop($page_des2);?>
		</div>
	</div>
	<?php }?>

    <div class="container">
        <div class="contact-form after-clear">
            <?php the_content();?>
        </div>
    </div>
    <?php endwhile;?>
</div>
<?php get_footer();?>