<?php 
/*
Template name: Home
*/
?>
<?php get_header();?>
<?php global $post, $osvn_opt;?>
<div id="index">

		<?php 
		$entries1 = get_post_meta( get_the_ID(), '_osvn_home_box', true );
		if(isset($entries1) && !empty($entries1)){
		?>
		<div class="featured">
			<div class="container">
				<ul>
					<?php foreach ( (array) $entries1 as $key => $entry ) {?>
					<li><div class="table-middle"><a href="<?php echo $entry['_osvn_home_box_link'];?>"><?php echo $entry['_osvn_home_box_title'];?></a></div></li>
					<?php }?>
				</ul>
			</div>
		</div>
		<?php }?>

		<div class="container">

			<?php 
			$entries2 = get_post_meta( get_the_ID(), '_osvn_home2_box_des', true );
			if(isset($entries2) && !empty($entries2)){
			?>
			<div class="fl left">
				<h2><?php echo get_post_meta( get_the_ID(), '_osvn_home2_box_title', true );?></h2>
				<?php echo wpautop($entries2);?>
				<a href="<?php echo get_post_meta( get_the_ID(), '_osvn_home2_box_url', true );?>" class="common_btn">READ MORE</a>
			</div>
			<?php }?>

			<div class="fr right">
				<?php if(function_exists('osvn_feacebook_feed')){osvn_feacebook_feed();}?>
			</div>
		</div><!--/.container -->
		
		<?php 
		if(isset($osvn_opt['hotline']) && !empty($osvn_opt['hotline'])){
			$string = $osvn_opt['hotline'];
			$pattern = '/[^0-9]*/';
			$result = preg_replace($pattern,'', $string);
		?>
		<div id="phone-call" class="text-center">
			<a href="tel:<?php echo $result;?>" class="block icon-phone"></a>
			call for a <span>free</span>
			<br>no obligation, consultation
			<div class="number"><?php echo $osvn_opt['hotline'];?></div>
		</div>
		<?php }?>
		
		<?php 
		$entries3 = get_post_meta( get_the_ID(), '_osvn_home_box3', true );
		if(isset($entries3) && !empty($entries3)){
		?>
		<div class="member-ship">
			<div class="container">
				<h3 class="title">Professional Memberships</h3>
				<ul class="rows">
					<?php foreach ( (array) $entries3 as $key => $entry ) {?>
					<li class="col-md-4">
						<a href="<?php echo $entry['_osvn_home3_box_link'];?>" target="_blank">
							<img src="<?php echo $entry['_osvn_home3_box_image'];?>" class="res-img" alt="">
							<?php 
							if(!empty($entry['_osvn_home3_box_title'])){
								echo '<span>'.$entry['_osvn_home3_box_title'].'</span>';
							}
							?>
						</a>
					</li>
					<?php }?>
				</ul>
			</div>
		</div>
		<?php }?>

	</div><!--/#index -->
<?php get_footer();?>