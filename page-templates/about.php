<?php 
/*
Template name: About
*/
?>
<?php get_header();?>
<?php 
global $post;
if(has_post_thumbnail()){
?>
<div class="back-slider">
    <?php the_post_thumbnail('page-thumb', array('class'=>'res-img'));?>
</div>
<?php }?>

<div id="main">
    <?php while(have_posts()): the_post();?>
    <div class="large-title">
        <div class="container">
                <h2><?php single_post_title();?></h2>
                <?php 
                $page_des = get_post_meta( get_the_ID(), '_osvn_page_des', true );
                if(isset($page_des) && !empty($page_des)){
                    echo wpautop($page_des);
                }
                ?>
        </div>
    </div>
    <div class="container">
        <div class="text-content">
            <?php the_content();?>
        </div>
    </div>
    <div class="member-ship">
        <div class="container">
            <?php 
            $bottom = get_post_meta(get_the_ID(), '_osvn_about_des', true);
            if(!empty($bottom)){
                echo wpautop($bottom);
            }
            ?>
        </div>
    </div>
    <?php endwhile;?>
</div>
<?php get_footer();?>