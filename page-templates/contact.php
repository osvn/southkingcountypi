<?php 
/*
Template name: Contact
*/
?>
<?php get_header();?>
<?php 
global $post;
if(has_post_thumbnail()){
?>
<div class="back-slider">
    <?php the_post_thumbnail('page-thumb', array('class'=>'res-img'));?>
</div>
<?php }?>

<div id="main">
		<div class="container">
			<div class="special contact-form after-clear">
				<?php while(have_posts()): the_post();?>
				<?php the_content();?>
				<?php endwhile;?>
			</div>
		</div>
	</div>
<?php get_footer();?>