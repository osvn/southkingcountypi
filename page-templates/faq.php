<?php 
/*
Template name: FAQ
*/
?>
<?php get_header();?>
<?php 
global $post;
if(has_post_thumbnail()){
?>
<div class="back-slider">
    <?php the_post_thumbnail('page-thumb', array('class'=>'res-img'));?>
</div>
<?php }?>

<div id="main">
    <div class="large-title">
        <div class="container">
                <h2><?php single_post_title();?></h2>
                <?php 
                $page_des = get_post_meta( get_the_ID(), '_osvn_page_des', true );
                if(isset($page_des) && !empty($page_des)){
                    echo wpautop($page_des);
                }
                ?>
        </div>
    </div>
    <div class="container">
        <?php 
        $args =  array('post_type'=>'osvn_faq', 'posts_per_page'=>50);
        $wq = new WP_Query($args);
        if($wq->have_posts()):
        ?>
        <ul id="faq">
            <?php while($wq->have_posts()): $wq->the_post();?>
            <li>
                <h4 class="question"><span>Q:</span> <?php the_title();?></h4>
                <div class="answer"><?php the_content();?></div>
            </li>
            <?php endwhile;?>
        </ul>
        <?php endif;wp_reset_query();?>
    </div>
</div>
<?php get_footer();?>