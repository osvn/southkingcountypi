<?php 
/*
Template name: Links
*/
?>
<?php get_header();?>
<?php 
global $post;
if(has_post_thumbnail()){
?>
<div class="back-slider">
    <?php the_post_thumbnail('page-thumb', array('class'=>'res-img'));?>
</div>
<?php }?>

<div id="main">
	<?php while(have_posts()): the_post();?>
        <div class="large-title">
            <div class="container">
                    <h2><?php single_post_title();?></h2>
                    <?php 
                    $page_des = get_post_meta( get_the_ID(), '_osvn_page_des', true );
                    if(isset($page_des) && !empty($page_des)){
                        echo wpautop($page_des);
                    }
                    ?>
            </div>
        </div>

        
        <?php 
        $entries = get_post_meta( get_the_ID(), '_osvn__link', true );
        if(isset($entries) && !empty($entries)){
        ?>
        <ul class="links-group">
            <?php foreach ( (array) $entries as $key => $entry ) {?>
            <li>
                <h2>
                    <div class="container"><?php echo $entry['_osvn_link_title'];?></div>
                </h2>
                <div class="container">
                    <?php echo wpautop($entry['_osvn_link_content']);?>
                </div>
            </li>
            <?php }?>
        </ul>
        <?php }?>

    <?php endwhile;?>
</div>
<?php get_footer();?>