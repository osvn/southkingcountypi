jQuery(document).ready(function(){
	/* Perfect Accordition */
	jQuery.each(jQuery('#faq .answer'), function() {
		jQuery(this).css('height',jQuery(this).height() + 36);
	});
	jQuery('#faq li').addClass('close-acc');
	jQuery('#faq h4').click(function(){
		jQuery(this).parent().toggleClass('close-acc')
	});
	
})
jQuery(window).bind("load", function() {
	jQuery('.navbar-main').height(jQuery('body').height());
	jQuery('.navbar-toggle').on( "click", function(){
		if (jQuery('.navbar-main').hasClass('active'))
			jQuery('.navbar-main').removeClass('active');
		else
			jQuery('.navbar-main').addClass('active');
	});
	jQuery('.navbar-main .close-btn').click(function(){
		jQuery('.navbar-main').removeClass('active');
		jQuery('#navbar').removeClass('in');
	});
});